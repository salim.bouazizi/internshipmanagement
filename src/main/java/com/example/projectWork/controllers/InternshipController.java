package com.example.projectWork.controllers;


import com.example.projectWork.dto.InternshipRequest;
import com.example.projectWork.dto.InternshipResponse;
import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import com.example.projectWork.interfaces.IInternshipService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
@RestController
@RequestMapping("/api/v1/internship")
@RequiredArgsConstructor
@CrossOrigin("*")
public class InternshipController {

    private final IInternshipService internshipService;



    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<InternshipResponse> createNewInternship(@RequestBody @Valid InternshipRequest request){
        return internshipService.createNewInternship(request);
    }
    @GetMapping("/{id_internship}")
    @ResponseStatus(HttpStatus.FOUND)
    public InternshipResponse getInternshipByID(@PathVariable int id_internship){
        return internshipService.getInternshipById(id_internship);
    }
    @GetMapping("/company/{id_company}")
    @ResponseStatus(HttpStatus.FOUND)
    public List<InternshipResponse> getAllInternshipByCompany(@PathVariable int id_company){
        return internshipService.getAllInternshipByCompany(id_company);
    }
    @GetMapping("/internship-status")
    @ResponseStatus(HttpStatus.FOUND)
    public List<InternshipResponse> getAllInternshipByStatus(@RequestParam("status")InternshipStatus status){
        return internshipService.getAllintershipByStatus(status);
    }
    @GetMapping("/internship-type")
    @ResponseStatus(HttpStatus.FOUND)
    public List<InternshipResponse> getAllInternshipByType(@RequestParam("type") InternshipType type){
        return internshipService.getAllintershipByType(type);
    }
    @GetMapping("/supervisor")
    @ResponseStatus(HttpStatus.FOUND)
    public List<InternshipResponse> getAllintershipBySupervisor(@RequestParam("id_supervisor") int id_supervisor){
        return internshipService.getAllintershipBySupervisor(id_supervisor);
    }
    @GetMapping("date-between")
    @ResponseStatus(HttpStatus.FOUND)
    public List<InternshipResponse> getAllInternshipAfternDate(@RequestParam("start-date") @DateTimeFormat(pattern = "yyyy-mm-dd")Date start_date ){
        return internshipService.getAllInternshipAfternDate(start_date);
    }
    @PutMapping("/set-internship/{id_internship}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public InternshipResponse setInternship(@PathVariable int id_internship,@RequestBody @Valid InternshipRequest request){
        return internshipService.setInternship(id_internship,request);
    }
    @DeleteMapping("/{id_internship}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InternshipResponse> deleteInternship(@PathVariable int id_internship,@RequestParam("id_supervisor") int id_supervisor){
        return internshipService.deleteInternship(id_internship,id_supervisor);
    }





}
