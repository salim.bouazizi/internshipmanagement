package com.example.projectWork.controllers;


import com.example.projectWork.dto.CompanyRequest;
import com.example.projectWork.dto.CompanyResponse;
import com.example.projectWork.enums.CompanyStatus;
import com.example.projectWork.enums.Domaine;
import com.example.projectWork.interfaces.ICompanyServices;
import com.example.projectWork.interfaces.IUserService;
import com.example.projectWork.models.Company;
import com.example.projectWork.models.User;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("/api/v1/company")
@RequiredArgsConstructor
@CrossOrigin("*")
public class CompanyController {
    private final IUserService iUserService;
    private final ICompanyServices iCompanyServices;
    @PostMapping("/Create")
    public ResponseEntity<Company> addCompany(@RequestBody @Valid CompanyRequest request) throws MessagingException {
        User supervisor = iUserService.findUserById(request.getSupervisorId()).orElseThrow(()-> new RuntimeException("SuperVisor NOT found"));
        return iCompanyServices.addCompany(supervisor,request);
    }
    @GetMapping("/{id_company}")
    public Company getCompanyById(@PathVariable int id_company){
        return iCompanyServices.findCompanyById(id_company);
    }

    @GetMapping("/status")
    @ResponseStatus(HttpStatus.FOUND)
    public List<CompanyResponse> getAllCompanyByStatus(@RequestParam("status") CompanyStatus status){
        return iCompanyServices.getAllCompanyByStatus(status);
    }
    @PutMapping("/set-status/{id_company}")
    public ResponseEntity<Company> setCompanyStatus(@PathVariable int id_company, @RequestParam("newStatus")CompanyStatus companyStatus) throws MessagingException {
        return iCompanyServices.setCompanyStatus(id_company,companyStatus);
    }
    @PutMapping("/set-company/{id_company}")
    public ResponseEntity<Company> setCompany(@PathVariable int id_company, @RequestBody @Valid CompanyRequest request )  {
        return iCompanyServices.setCompany(id_company,request);
    }
    @GetMapping("/domaine")
    @ResponseStatus(HttpStatus.FOUND)
    public List<CompanyResponse> getAllCompanyByDomaine(@RequestParam("domaine")Domaine domaine){
        return iCompanyServices.getAllCompanyByDomaine(domaine);
    }






}
