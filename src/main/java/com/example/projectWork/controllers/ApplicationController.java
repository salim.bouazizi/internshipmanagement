package com.example.projectWork.controllers;


import com.example.projectWork.dto.ApplicationRequest;
import com.example.projectWork.dto.ApplicationResponse;
import com.example.projectWork.enums.ApplicationStatus;
import com.example.projectWork.interfaces.IApplicationServices;
import com.example.projectWork.models.Application;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/application")
@CrossOrigin("*")
public class ApplicationController {

    private final IApplicationServices applicationServices;

    @PostMapping("/addNewApplication")
    @ResponseStatus(HttpStatus.CREATED)
    public ApplicationResponse addNewApplication(@RequestBody @Valid ApplicationRequest request){
        return applicationServices.addNewApplication(request);
    }
    @GetMapping("/{id_application}")
    @ResponseStatus(HttpStatus.FOUND)
    private ApplicationResponse getOneApplication(@PathVariable int id_application){
        return applicationServices.getOneApplication(id_application);
    }
    @GetMapping("/user/{id_user}")
    @ResponseStatus(HttpStatus.FOUND)
    public List<ApplicationResponse> getApplicationByUser(@PathVariable int id_user){
        return applicationServices.getApplicationByUser(id_user);
    }
    @GetMapping("/internship/{id_internship}")
    @ResponseStatus(HttpStatus.FOUND)
    public List<ApplicationResponse> getApplicationByInternship(@PathVariable int id_internship){
        return applicationServices.getApplicationByInternship(id_internship);
    }
    @GetMapping("/company/{id_company}")
    @ResponseStatus(HttpStatus.FOUND)
    public List<ApplicationResponse> getApplicationByCompany(@PathVariable int id_company){
        return applicationServices.getApplicationByCompany(id_company);
    }
    @PutMapping("/set-Status/{id_application}")
    @ResponseStatus(HttpStatus.OK)
    public ApplicationResponse setApplicationStatus(@PathVariable int id_application, @RequestParam("newStatus")ApplicationStatus status) throws MessagingException {
        return applicationServices.setApplicationResponse(id_application,status);
    }
    @GetMapping("/status")
    @ResponseStatus(HttpStatus.FOUND)
    public List<ApplicationResponse> getApplicationbyStatus(@RequestParam("status") ApplicationStatus status){
        return applicationServices.getApplicationByStatus(status);
    }
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ApplicationResponse> deleteApplication(@RequestParam("id_application") int id_application,@RequestParam("id_student") int id_student){
        return applicationServices.deleteApplication(id_application,id_student);
    }



}
