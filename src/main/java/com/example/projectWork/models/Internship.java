package com.example.projectWork.models;

import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Internship implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int intershipId;
    private String postName;
    private String postDescription;
    private int postCapacity;
    private Date startDate;
    private Date endDate;
    @Enumerated(EnumType.STRING)
    private InternshipType internshipType;
    @Enumerated(EnumType.STRING)
    private InternshipStatus internshipStatus;
    private LocalDateTime creationDate;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "supervisor_id")
    private User supervisor;
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

}
