package com.example.projectWork.dto;
import com.example.projectWork.enums.InternshipType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InternshipRequest {
    @NotEmpty(message = "postName is mandatory")
    @NotBlank(message = "postName is mandatory")
    private String postName;
    @NotEmpty(message = "postDescription is mandatory")
    @NotBlank(message = "postDescription is mandatory")
    private String postDescription;
    @NotEmpty(message = "postCapacity is mandatory")
    @NotBlank(message = "postCapacity is mandatory")
    @Size(min = 1)
    private int postCapacity;
    @NotEmpty(message = "startDate is mandatory")
    @NotBlank(message = "startDate is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotEmpty(message = "endDate is mandatory")
    @NotBlank(message = "endDate is mandatory")
    private Date endDate;
    @NotEmpty(message = "internshipType is mandatory")
    @NotBlank(message = "internshipType is mandatory")
    private InternshipType internshipType;
    @NotEmpty(message = "supervisorId is mandatory")
    @NotBlank(message = "supervisorId is mandatory")
    private int supervisorId;
    @NotEmpty(message = "companyId is mandatory")
    @NotBlank(message = "companyId is mandatory")
    private int companyId;
    @NotEmpty(message = "companyCode is mandatory")
    @NotBlank(message = "companyCode is mandatory")
    private int companyCode;


}
