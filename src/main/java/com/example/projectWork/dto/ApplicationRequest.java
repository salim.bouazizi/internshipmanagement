package com.example.projectWork.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationRequest {
    @NotEmpty(message = "studentId is mandatory")
    @NotBlank(message = "studentId is mandatory")
    private int studentId;
    @NotEmpty(message = "internshipId is mandatory")
    @NotBlank(message = "internshipId is mandatory")
    private int internshipId;
}

