package com.example.projectWork.dto;
import com.example.projectWork.enums.Domaine;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompanyRequest {
    private int supervisorId;
    @NotEmpty(message = "CompanyName is mandatory")
    @NotBlank(message = "CompanyName is mandatory")
    private String companyName;
    @NotEmpty(message = "Address is mandatory")
    @NotBlank(message = "Address is mandatory")
    private String address;
    @NotEmpty(message = "Domaine is mandatory")
    @NotBlank(message = "Domaine is mandatory")
    private Domaine domaine;
    @NotEmpty(message = "Website is mandatory")
    @NotBlank(message = "Website is mandatory")
    private String webSite;
    @NotEmpty(message = "Email is mandatory")
    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email is not formatted")
    private String companyEmail;
    @NotEmpty(message = "Telephone number is mandatory")
    @NotBlank(message = "Telephone number is mandatory")
    @Size(min = 8 ,max = 8 ,message = "Max telephone number is 8")
    private int companyTelephone;

}
