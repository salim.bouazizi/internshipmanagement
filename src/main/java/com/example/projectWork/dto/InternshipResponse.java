package com.example.projectWork.dto;
import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InternshipResponse {
    private int internshipId;
    private String postName;
    private String postDescription;
    private int postCapacity;
    private Date startDate;
    private Date endDate;
    private InternshipStatus internshipStatus;
    private LocalDateTime creationDate;
    private InternshipType internshipType;
    private int supervisorId;
    private int companyId;


}
