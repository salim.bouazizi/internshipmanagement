package com.example.projectWork.dto;

import com.example.projectWork.enums.Domaine;
import com.example.projectWork.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompanyResponse {
    private int CompanyId;
    private String companyName;
    private String address;
    private Domaine domaine;
    private String webSite;
    private String companyEmail;
    private int companyTelephone;


}
