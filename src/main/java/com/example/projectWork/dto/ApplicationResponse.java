package com.example.projectWork.dto;

import com.example.projectWork.enums.ApplicationStatus;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationResponse {
    private int applicationId;
    private LocalDateTime applicationDate;
    private ApplicationStatus applicationStatus;
    private User student;
    private int internshipId;
}
