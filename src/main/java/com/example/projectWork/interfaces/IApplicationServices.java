package com.example.projectWork.interfaces;

import com.example.projectWork.dto.ApplicationRequest;
import com.example.projectWork.dto.ApplicationResponse;
import com.example.projectWork.enums.ApplicationStatus;
import com.example.projectWork.models.Application;
import jakarta.mail.MessagingException;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.List;

public interface IApplicationServices  {

    ApplicationResponse addNewApplication(ApplicationRequest request);

    ApplicationResponse getOneApplication(int idApplication);

    List<ApplicationResponse> getApplicationByInternship(int idInternship);

    ApplicationResponse setApplicationResponse(int idApplication, ApplicationStatus status) throws MessagingException;

    List<ApplicationResponse> getApplicationByCompany(int idCompany);

    List<ApplicationResponse> getApplicationByStatus(ApplicationStatus status);

    ResponseEntity<ApplicationResponse> deleteApplication(int idApplication, int idStudent);

    List<ApplicationResponse> getApplicationByUser(int idUser);
}
