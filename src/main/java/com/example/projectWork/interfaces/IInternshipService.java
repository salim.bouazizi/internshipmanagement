package com.example.projectWork.interfaces;

import com.example.projectWork.dto.InternshipRequest;
import com.example.projectWork.dto.InternshipResponse;
import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import com.example.projectWork.models.Company;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

public interface IInternshipService {
    ResponseEntity<InternshipResponse> createNewInternship(InternshipRequest request);

    InternshipResponse getInternshipById(int idInternship);

    List<InternshipResponse> getAllInternshipByCompany(int idCompany);

    List<InternshipResponse> getAllintershipByStatus(InternshipStatus status);

    List<InternshipResponse> getAllintershipByType(InternshipType type);

    List<InternshipResponse> getAllintershipBySupervisor(int idSupervisor);

    InternshipResponse setInternship(int idInternship, InternshipRequest request);

    ResponseEntity<InternshipResponse> deleteInternship(int idInternship, int idSupervisor);


    List<InternshipResponse> getAllInternshipAfternDate(Date startDate);
}
