package com.example.projectWork.serviceImplementation;

import com.example.projectWork.Repository.UserRepository;
import com.example.projectWork.dto.StudentDetailsRequest;
import com.example.projectWork.enums.ClassLevel;
import com.example.projectWork.enums.Role;
import com.example.projectWork.enums.Speciality;
import com.example.projectWork.enums.StudentLevel;
import com.example.projectWork.interfaces.IUserService;
import com.example.projectWork.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class UserService implements IUserService {
    private final UserRepository userRepository;
    @Override
    public Optional<User> findUserById(int idUser) {
        return userRepository.findById(idUser);
    }

    @Override
    public List<User> getAllUserByRole(Role role) {
        return userRepository.findAllByRole(role)  ;
    }

    @Override
    public List<User> getAllUserBySpeciality(Speciality speciality) {
        return userRepository.findAllBySpecialty(speciality);
    }

    @Override
    public List<User> getAllUserByStudentLevel(StudentLevel studentlevel) {
        return userRepository.findAllByStudentLevel(studentlevel);
    }

    @Override
    public List<User> getAllUserByClasseLevel(ClassLevel classLevel) {
        return userRepository.findAllByClasseLevel(classLevel);
    }

    @Override
    public void setUserImage(int idUser, MultipartFile image) throws IOException {
        User user = userRepository.findById(idUser).orElseThrow(()->new RuntimeException("User NOT FOUND "));
        user.setUserImage(image.getBytes());
        userRepository.save(user);
    }

    @Override
    public ResponseEntity<User> setStudentProfile(int id_user, StudentDetailsRequest request) {
        User user=userRepository.findById(id_user).get();
        user.setBirthdate(request.getBirthdate());
        user.setStudentLevel(request.getLevel());
        user.setStudentCV(request.getStudentCV());
        user.setSpecialty(request.getSpecialty());
        user.setUserImage(request.getUserImage());
        user.setClasseLevel(request.getClasse());
        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

}
