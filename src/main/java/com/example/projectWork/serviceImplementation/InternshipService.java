package com.example.projectWork.serviceImplementation;

import com.example.projectWork.Repository.CompanyRepository;
import com.example.projectWork.Repository.InternshipRepository;
import com.example.projectWork.dto.InternshipRequest;
import com.example.projectWork.dto.InternshipResponse;
import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import com.example.projectWork.interfaces.ICompanyServices;
import com.example.projectWork.interfaces.IInternshipService;
import com.example.projectWork.interfaces.IUserService;
import com.example.projectWork.models.Company;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InternshipService implements IInternshipService {
    private final IUserService userService;
    private final ICompanyServices companyServices;
    private final InternshipRepository repository;
    private final CompanyRepository companyRepository;

    @Override
    public ResponseEntity<InternshipResponse> createNewInternship(InternshipRequest request) {

        User supervisor = userService.findUserById(request.getSupervisorId()).orElseThrow(()->new RuntimeException("User NOT FOUND"));

        Company company = companyServices.findCompanyById(request.getCompanyId());
        if(company==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(company.getCompanyPrintCode()!= request.getCompanyCode() || company.getSupervisor()!= supervisor){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
            Internship internship = Internship
                    .builder()
                    .supervisor(supervisor)
                    .company(company)
                    .internshipStatus(InternshipStatus.OPEN)
                    .internshipType(request.getInternshipType())
                    .postCapacity(request.getPostCapacity())
                    .startDate(request.getStartDate())
                    .endDate(request.getEndDate())
                    .postDescription(request.getPostDescription())
                    .postName(request.getPostName())
                    .creationDate(LocalDateTime.now())
                    .build();
             repository.save(internship);
             InternshipResponse internshipResponse = InternshipResponse
                     .builder()
                     .internshipId(internship.getIntershipId())
                     .companyId(internship.getCompany().getCompanyId())
                     .startDate(internship.getStartDate())
                     .endDate(internship.getEndDate())
                     .postCapacity(internship.getPostCapacity())
                     .postName(internship.getPostName())
                     .postDescription(internship.getPostDescription())
                     .internshipType(internship.getInternshipType())
                     .supervisorId(internship.getSupervisor().getUserId())
                     .internshipStatus(internship.getInternshipStatus())
                     .creationDate(internship.getCreationDate())
                     .build();
            return new ResponseEntity<>(internshipResponse,HttpStatus.CREATED);
    }

    @Override
    public InternshipResponse getInternshipById(int idInternship) {
        Internship internship= repository.findById(idInternship).orElseThrow(()->new RuntimeException("Internship NOT FOUND"));
        return InternshipResponse
                .builder()
                .internshipId(internship.getIntershipId())
                .companyId(internship.getCompany().getCompanyId())
                .startDate(internship.getStartDate())
                .endDate(internship.getEndDate())
                .postCapacity(internship.getPostCapacity())
                .postName(internship.getPostName())
                .postDescription(internship.getPostDescription())
                .internshipType(internship.getInternshipType())
                .supervisorId(internship.getSupervisor().getUserId())
                .internshipStatus(internship.getInternshipStatus())
                .creationDate(internship.getCreationDate())
                .build();
    }

    @Override
    public List<InternshipResponse> getAllInternshipByCompany(int idCompany) {
        Company company= companyRepository.findById(idCompany).orElseThrow(()->new RuntimeException("Company Not Found"));
        List<Internship> internshipList = repository.findAllByCompany(company);
        return internshipList.stream().map(this::mapToInternshipResponse).toList();
    }

    @Override
    public List<InternshipResponse> getAllintershipByStatus(InternshipStatus status) {
        List<Internship> internshipList = repository.findAllByInternshipStatus(status);
        return internshipList.stream().map(this::mapToInternshipResponse).toList();
    }

    @Override
    public List<InternshipResponse> getAllintershipByType(InternshipType type) {
        List<Internship> internshipList = repository.findAllByInternshipType(type);
        return internshipList.stream().map(this::mapToInternshipResponse).toList();    }

    @Override
    public List<InternshipResponse> getAllintershipBySupervisor(int idSupervisor) {
        User supervisor = userService.findUserById(idSupervisor).orElseThrow(()->new RuntimeException("User NOT FOUND"));
        List<Internship> internshipList = repository.findAllBySupervisor(supervisor);
        return internshipList.stream().map(this::mapToInternshipResponse).toList();
    }

    @Override
    public InternshipResponse setInternship(int idInternship, InternshipRequest request) {
        User supervisor= userService.findUserById(request.getSupervisorId()).orElseThrow(()->new RuntimeException("Supervisor NOT FOUND"));
        Internship internship = repository.findById(idInternship).orElseThrow(()->new RuntimeException("Internship NOT FOUND"));
        if(internship.getSupervisor().equals(supervisor)){
            internship.setPostName(request.getPostName());
            internship.setPostCapacity(request.getPostCapacity());
            internship.setPostDescription(request.getPostDescription());
            internship.setStartDate(request.getStartDate());
            internship.setEndDate(request.getEndDate());
            internship.setInternshipType(request.getInternshipType());
            repository.save(internship);
            return InternshipResponse
                    .builder()
                    .internshipId(internship.getIntershipId())
                    .companyId(internship.getCompany().getCompanyId())
                    .startDate(internship.getStartDate())
                    .endDate(internship.getEndDate())
                    .postCapacity(internship.getPostCapacity())
                    .postName(internship.getPostName())
                    .postDescription(internship.getPostDescription())
                    .internshipType(internship.getInternshipType())
                    .supervisorId(internship.getSupervisor().getUserId())
                    .internshipStatus(internship.getInternshipStatus())
                    .creationDate(internship.getCreationDate())
                    .build();
        }else {
            throw new RuntimeException("you dond have the permission to edit this internship ");
        }
    }

    @Override
    public ResponseEntity<InternshipResponse> deleteInternship(int idInternship, int idSupervisor) {
        User supervisor= userService.findUserById(idSupervisor).orElseThrow(()->new RuntimeException("Supervisor NOT FOUND"));
        Internship internship = repository.findById(idInternship).orElseThrow(()->new RuntimeException("Internship NOT FOUND"));
        if(internship.getSupervisor().equals(supervisor)){
            internship.setSupervisor(null);
            internship.setCompany(null);
            repository.delete(internship);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            throw new RuntimeException("you dont have the permission to delete this internship");
        }
    }

    @Override
    public List<InternshipResponse> getAllInternshipAfternDate(Date startDate) {
                List<Internship> internshipList = repository.findAllByStartDateAfter(startDate);
                return internshipList.stream().map(this::mapToInternshipResponse).toList();
    }


    private InternshipResponse mapToInternshipResponse(Internship internship) {
        return InternshipResponse
                .builder()
                .internshipId(internship.getIntershipId())
                .companyId(internship.getCompany().getCompanyId())
                .startDate(internship.getStartDate())
                .endDate(internship.getEndDate())
                .postCapacity(internship.getPostCapacity())
                .postName(internship.getPostName())
                .postDescription(internship.getPostDescription())
                .internshipType(internship.getInternshipType())
                .supervisorId(internship.getSupervisor().getUserId())
                .internshipStatus(internship.getInternshipStatus())
                .creationDate(internship.getCreationDate())
                .build();
    }
}
