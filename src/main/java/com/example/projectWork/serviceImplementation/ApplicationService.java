package com.example.projectWork.serviceImplementation;


import com.example.projectWork.Repository.ApplicationRepository;
import com.example.projectWork.Repository.CompanyRepository;
import com.example.projectWork.Repository.InternshipRepository;
import com.example.projectWork.Repository.UserRepository;
import com.example.projectWork.dto.ApplicationRequest;
import com.example.projectWork.dto.ApplicationResponse;
import com.example.projectWork.email.EmailService;
import com.example.projectWork.email.EmailTemplateName;
import com.example.projectWork.enums.ApplicationStatus;
import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.Role;
import com.example.projectWork.interfaces.IApplicationServices;
import com.example.projectWork.models.Application;
import com.example.projectWork.models.Company;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicationService implements IApplicationServices {

    private final UserRepository userRepository;
    private final InternshipRepository internshipRepository;
    private final ApplicationRepository applicationRepository;
    private final CompanyRepository companyRepository;
    @Autowired
    private final EmailService emailService;
    @Override
    public ApplicationResponse addNewApplication(ApplicationRequest request) {
        User student = userRepository.findById(request.getStudentId()).orElseThrow(()-> new RuntimeException("User NOT Found"));
        Internship internship = internshipRepository.findById(request.getInternshipId()).orElseThrow(()->new RuntimeException("Internship NOT Found"));

           Application application = Application
                   .builder()
                   .applicationDate(LocalDateTime.now())
                   .applicationStatus(ApplicationStatus.PENDING)
                   .student(student)
                   .internship(internship)
                   .build();

           applicationRepository.save(application);
             return ApplicationResponse
                .builder()
                .student(application.getStudent())
                .applicationId(application.getApplicationId())
                .applicationStatus(application.getApplicationStatus())
                .applicationDate(application.getApplicationDate())
                .internshipId(application.getInternship().getIntershipId())
                .build();



    }

    @Override
    public ApplicationResponse getOneApplication(int idApplication) {
         Application application = applicationRepository.findById(idApplication).orElseThrow(()->new RuntimeException("Application NOT FOUND"));
         return ApplicationResponse
                 .builder()
                 .student(application.getStudent())
                 .applicationId(application.getApplicationId())
                 .applicationStatus(application.getApplicationStatus())
                 .applicationDate(application.getApplicationDate())
                 .internshipId(application.getInternship().getIntershipId())
                 .build();
    }

    @Override
    public List<ApplicationResponse> getApplicationByInternship(int idInternship) {
        Internship internship = internshipRepository.findById(idInternship).orElseThrow(()->new RuntimeException("internship NOT FOUND"));
        List<Application> applicationList = applicationRepository.findAllByInternship(internship);
        return applicationList.stream().map(this::mapToApplicationResponse).toList();
    }

    @Override
    public ApplicationResponse setApplicationResponse(int idApplication, ApplicationStatus status) throws MessagingException {

        Application application = applicationRepository.findById(idApplication).orElseThrow(()->new RuntimeException("Application NOT FOUND"));
        Internship internship = internshipRepository.findById(application.getInternship().getIntershipId()).orElseThrow(()->new RuntimeException("Internship NOT FOUND"));
        if(internship.getInternshipStatus().equals(InternshipStatus.CLOSE)){
            throw new RuntimeException("this Post has been closed because it acheve the max value of application");
        }else{
            int nbrPostValide=internship.getPostCapacity();
            if (status.equals(ApplicationStatus.ACCEPTED)){
                application.setApplicationStatus(ApplicationStatus.ACCEPTED);
                nbrPostValide=nbrPostValide-1;
                internship.setPostCapacity(nbrPostValide);
                if(nbrPostValide==0){
                    internship.setInternshipStatus(InternshipStatus.CLOSE);
                }
                internshipRepository.save(internship);
                applicationRepository.save(application);
              //  emailService.sendApplicationConfirmationEmail(application.getStudent().getEmail(),application.getStudent().fullName(), EmailTemplateName.APPLICATION_ACCEPTED,"Application has been ACCEPTED");
                return mapToApplicationResponse(application);
            }else{
                application.setApplicationStatus(ApplicationStatus.REFUSED);
                //emailService.sendApplicationConfirmationEmail(application.getStudent().getEmail(),application.getStudent().fullName(), EmailTemplateName.APPLICATION_REFUSED,"Application has been ACCEPTED");                applicationRepository.save(application);
                return mapToApplicationResponse(application);
            }
        }

    }

    @Override
    public List<ApplicationResponse> getApplicationByCompany(int idCompany) {
        Company company = companyRepository.findById(idCompany).orElseThrow(()->new RuntimeException("company NOT FOUND"));
        List<Application> applicationList = applicationRepository.findallApplicationByCompany(idCompany);
        return applicationList.stream().map(this::mapToApplicationResponse).toList();
    }

    @Override
    public List<ApplicationResponse> getApplicationByStatus(ApplicationStatus status) {
        List<Application> applicationList = applicationRepository.findAllByApplicationStatus(status);
        return applicationList.stream().map(this::mapToApplicationResponse).toList();
    }

    @Override
    public ResponseEntity<ApplicationResponse> deleteApplication(int idApplication, int idStudent) {
        int nbrPostValide;
        User user = userRepository.findById(idStudent).orElseThrow(()->new RuntimeException("User NOT FOUND"));
        Application application = applicationRepository.findById(idApplication).orElseThrow(()->new RuntimeException("Application NOT FOUND"));
        if(application.getStudent().getUserId()==(user.getUserId())){
            Internship internship = internshipRepository.findById(application.getInternship().getIntershipId()).orElseThrow(()->new RuntimeException("Internship NOT FOUND"));
            nbrPostValide=internship.getPostCapacity();
            nbrPostValide=nbrPostValide+1;
            if(internship.getInternshipStatus().equals(InternshipStatus.CLOSE)){
                internship.setInternshipStatus(InternshipStatus.OPEN);
            }
            internship.setPostCapacity(nbrPostValide);
            internshipRepository.save(internship);
            application.setInternship(null);
            application.setStudent(null);
            applicationRepository.delete(application);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

    }

    @Override
    public List<ApplicationResponse> getApplicationByUser(int idUser) {
        User user = userRepository.findById(idUser).orElseThrow(()->new RuntimeException("User NOT FOUND"));
        List<Application> applicationList = applicationRepository.findAllByStudent(user);
        return applicationList.stream().map(this::mapToApplicationResponse).toList();

    }

    private ApplicationResponse mapToApplicationResponse(Application application) {
        return ApplicationResponse
                .builder()
                .student(application.getStudent())
                .applicationId(application.getApplicationId())
                .applicationStatus(application.getApplicationStatus())
                .applicationDate(application.getApplicationDate())
                .internshipId(application.getInternship().getIntershipId())
                .build();
    }
}
