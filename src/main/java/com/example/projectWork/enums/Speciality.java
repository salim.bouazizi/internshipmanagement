package com.example.projectWork.enums;

public enum Speciality {
    ARCTIC,
    BI,
    GAMING,
    DS,
    TWIN
}
