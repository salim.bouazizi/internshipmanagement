package com.example.projectWork.enums;

public enum InternshipType {
    FULL_TIME, PART_TIME, ONSITE, REMOTE
}
