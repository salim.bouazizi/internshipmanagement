package com.example.projectWork.enums;

public enum ClassLevel {
    FIRST_YEAR, SECOND_YEAR, THIRD_YEAR, FOURTH_YEAR, FIFTH_YEAR
}
