package com.example.projectWork.enums;

public enum StudentLevel {
    LICENCE, MASTER, ENGINEER
}
