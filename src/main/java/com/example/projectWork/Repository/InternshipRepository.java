package com.example.projectWork.Repository;

import com.example.projectWork.enums.InternshipStatus;
import com.example.projectWork.enums.InternshipType;
import com.example.projectWork.models.Company;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface InternshipRepository extends JpaRepository<Internship,Integer> {
    List<Internship> findAllByCompany(Company company);

    List<Internship>  findAllByInternshipStatus (InternshipStatus status);

    List<Internship> findAllByInternshipType (InternshipType type);

    List<Internship> findAllBySupervisor (User supervisor);

    List<Internship> findAllByStartDateBetween(Date start_date, Date end_date);

    List<Internship> findAllByStartDateAfter (Date start_date);

}
