package com.example.projectWork.Repository;

import com.example.projectWork.dto.ApplicationResponse;
import com.example.projectWork.enums.ApplicationStatus;
import com.example.projectWork.models.Application;
import com.example.projectWork.models.Internship;
import com.example.projectWork.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<Application,Integer> {

    List<Application> findAllByApplicationStatus(ApplicationStatus applicationStatus);
    List<Application> findAllByInternship(Internship internship);

    @Query("select a from Application a where a.internship.company.companyId = :idCompany")
    List<Application> findallApplicationByCompany(int idCompany);

    List<Application> findAllByStudent (User student);
}
